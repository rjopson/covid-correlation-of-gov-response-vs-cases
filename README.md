# Covid Correlation of Gov Response vs Cases

Cases data source: https://www.nytimes.com/interactive/2020/us/coronavirus-us-cases.html#states

Government response source: https://github.com/OxCGRT/USA-covid-policy/blob/master/data/OxCGRT_US_latest.csv