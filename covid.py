import csv
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import r2_score

print('Data for recent cases vs government response')

def States():
    states = [  'Alabama',
                'Alaska',
                'Arizona',
                'Arkansas',
                'California',
                'Colorado',
                'Connecticut',
                'Delaware',
                #'Florida',
                'Georgia',
                'Hawaii',
                'Idaho',
                'Illinois',
                'Indiana',
                'Iowa',
                'Kansas',
                'Kentucky',
                'Louisiana',
                'Maine',
                'Maryland',
                'Massachusetts',
                'Michigan',
                'Minnesota',
                #'Mississippi',
                'Missouri',
                'Montana',
                'Nebraska',
                'Nevada',
                #'New Hampshire',
                'New Jersey',
                'New Mexico',
                'New York',
                'North Carolina',
                'North Dakota',
                'Ohio',
                'Oklahoma',
                'Oregon',
                'Pennsylvania',
                'Rhode Island',
                'South Carolina',
                'South Dakota',
                'Tennessee',
                'Texas',
                'Utah',
                'Vermont',
                'Virginia',
                'Washington',
                'West Virginia',
                'Wisconsin',
                'Wyoming']

    return states

def GetCases(file):
    dict_cases = {}    
    with open(file, encoding='utf-8', errors='ignore') as file_cases:
        data_cases = csv.reader(file_cases, delimiter=',')
        for row in data_cases:
            state = row[0]
            state = state.replace('+ ', '')
            state = state.replace(' MAP ', '')
            dict_cases[state] = row[4]

    return dict_cases

def GetResponse(file):
    dict_response = {}
    with open(file, encoding='utf-8', errors='ignore') as file_response:
        data_response = csv.reader(file_response, delimiter=',')
        for row in data_response:
            state = row[2]
            dict_response[state] = row[61]

        return dict_response

#Get data     
states = States()
dict_cases = GetCases('data_cases.csv')
dict_responses = GetResponse('data_response.csv')
cases = np.array([])
responses = np.array([])
for state in states:
    cases = np.append(cases, float(dict_cases[state]))
    responses = np.append(responses, float(dict_responses[state]))

#fit data
coef = np.polyfit(responses, cases,1)
res = np.polyfit(responses, cases,1, full=True)
poly1d_fn = np.poly1d(coef)
r2 = r2_score(cases, poly1d_fn(responses))

#plot data
fig, ax = plt.subplots()
ax.scatter(responses, cases)
ax.plot(responses, poly1d_fn(responses), '--k')
ax.set_title('R2: ' + str(r2))
plt.xlabel('Governement response (0 nothing, 100 a bunch of stuff)')
plt.ylabel('7 day avg, per 100,000')
for i, text in enumerate(states):
    ax.annotate(text, (responses[i], cases[i]))
plt.show()





